package com.PetPeersApplication.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.PetPeersApplication.exception.UserValidation;
import com.PetPeersApplication.model.Pet;
import com.PetPeersApplication.model.User;
import com.PetPeersApplication.service.PetService;
import com.PetPeersApplication.service.UserService;
import com.PetPeersApplication.service.UserServiceImpl;

@RestController
public class UserPetController {

	private static final Logger LOGGER = LogManager.getLogger(UserPetController.class);

	@Autowired
	private UserService userservice;

	@Autowired

	private PetService petservice;

	@PostMapping(value = "user/add")
	public ResponseEntity<User> addUser(@RequestBody User user) {
		ResponseEntity<User> responseEntity = null;
		try {
			responseEntity = new ResponseEntity<User>(userservice.addUser(user), HttpStatus.OK);
			LOGGER.info("User created successfully");
		} catch (UserValidation e) {

			LOGGER.error(e.getMessage());
		}

		return responseEntity;
	}

	@GetMapping(value = "user/show")
	public ResponseEntity<List<User>> getAllUsers() {

		List<User> users = userservice.listUsers();
		List<User> resultList = new ArrayList<User>();
		for (User user : users) {
			try {
				User allUsers = new User(user.getUserId(), user.getUserName(), user.getUserPassword());
				Set<Pet> pets = allUsers.getPets();
				Set<Pet> setOfpets = new HashSet<>();
				if (pets != null) {
					// LOGGER.info("pet" + user.getUserName());
					for (Pet petset : pets) {
						Pet resultpet = new Pet(petset.getId(), petset.getPetName(), petset.getPetAge(),
								petset.getPetPlace());
						setOfpets.add(resultpet);
						allUsers.setPets(setOfpets);
					}

				} else {

				}
				resultList.add(allUsers);
				
			} catch (NullPointerException e) {
				LOGGER.info(e);
			}
		}

		return new ResponseEntity<List<User>>(resultList, HttpStatus.OK);
	}

	@GetMapping(value = "user/login/{username}/{password}")
	public ModelAndView loginUser(@PathVariable("username") String username,
			@PathVariable("password") String password) {
		String result = null;
		User user;
		ModelAndView modelAndView = null;
		try {
			user = userservice.findByUserName(username);

			if (user.getUserPassword().equals(password)) {
				result = "Welcome " + user.getUserName();
				modelAndView = new ModelAndView("redirect:/Pets");
				LOGGER.info("Login Success");

			} else {
				LOGGER.error("Password is invalid");
			}
		} catch (UserValidation e) {
			LOGGER.error(e.getMessage());
		}
		return modelAndView;
	}

	@PostMapping(value = "Pets/addPet")
	public ModelAndView addPet(@RequestBody Pet pet) {
		ResponseEntity<Pet> responseEntity = null;
		ModelAndView modelAndView = null;

		try {
			// responseEntity = new ResponseEntity<Pet>(petservice.savePet(pet),
			// HttpStatus.OK);
			petservice.savePet(pet);
			modelAndView = new ModelAndView("redirect:/Pets");
			LOGGER.info("Pet created successfully");
		} catch (UserValidation e) {
			LOGGER.error(e.getMessage());
		}

		return modelAndView;
	}

	@GetMapping(value = "/Pets")
	public ResponseEntity<List<Pet>> petHome() {
		List<Pet> pets = petservice.getAllPets();
		List<Pet> resultList = new ArrayList<Pet>();
		for (Pet pet : pets) {
			try {
				Pet allPets = new Pet(pet.getId(), pet.getPetName(), pet.getPetAge(), pet.getPetPlace());
				User user = pet.getOwner();
				if (user != null) {
				
					User resultUser = new User(user.getUserId(), user.getUserName(), user.getUserPassword());
					allPets.setOwner(resultUser);

				} else {

				}
				resultList.add(allPets);

			} catch (NullPointerException e) {
				LOGGER.info(e);
			}
		}

		return new ResponseEntity<List<Pet>>(resultList, HttpStatus.OK);
	}

	@GetMapping(value = "Pets/buyPet/{petId}")
	public ResponseEntity<String> buyPet(@PathVariable("petId") long petId) {
		String result = null;
		Pet pet = petservice.getpetStatus(petId);
		if (pet != null) {
			userservice.buyPet(petId);
			result = "Pet " + pet.getPetName() + " bought successfully";
		} else {
			result = "Already sold";
		}
		
		return new ResponseEntity<String>(result, HttpStatus.OK);

	}

	@GetMapping(value = "Pets/myPets")
	public ResponseEntity<Set<Pet>> getmyPets() {
		Set<Pet> pets = userservice.getmyPets();

		Set<Pet> setOfPets = new HashSet<>();

		Pet resultPet = null;

		for (Pet pet : pets) {
			resultPet = new Pet(pet.getId(), pet.getPetName(), pet.getPetAge(), pet.getPetPlace());
			setOfPets.add(resultPet);
		}

		return new ResponseEntity<Set<Pet>>(setOfPets, HttpStatus.OK);
	}

	@GetMapping(value = "Pets/petDetail/{petId}")
	public ResponseEntity<Pet> petDetail(@PathVariable("petId") long petId) {
		Pet pet = petservice.getPetsById(petId);
		Pet resultPet = new Pet(pet.getId(), pet.getPetName(), pet.getPetAge(), pet.getPetPlace());
		return new ResponseEntity<Pet>(resultPet, HttpStatus.OK);
	}

	@GetMapping(value = "Pets/petStatus/{petId}")
	public ResponseEntity<String> petStatus(@PathVariable("petId") long petId) {
		String status = null;
		Pet pet = petservice.getpetStatus(petId);
		if (pet != null) {
			status = "Available to buy";
		} else {
			status = "Already sold";
		}
		return new ResponseEntity<String>(status, HttpStatus.OK);
	}

}
