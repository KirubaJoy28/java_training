package com.PetPeersApplication.service;

import java.util.List;

import com.PetPeersApplication.exception.UserValidation;
import com.PetPeersApplication.model.Pet;

public interface PetService {

	public abstract Pet savePet(Pet pet) throws UserValidation;
	
	public abstract Pet updatePet(Pet pet);
	
	public abstract List<Pet> getAllPets();
	
	public abstract Pet getPetsById(long petId);
	
	public abstract Pet getpetStatus(long petId);
}


