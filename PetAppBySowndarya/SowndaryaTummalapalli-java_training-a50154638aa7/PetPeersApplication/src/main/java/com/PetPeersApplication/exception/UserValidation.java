package com.PetPeersApplication.exception;

public class UserValidation extends Exception {

	private String message;

	
	public UserValidation() {
		super();
	}

	public UserValidation(String message) {
		super();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	
}
