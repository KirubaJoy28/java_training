package com.PetPeersApplication.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.PetPeersApplication.dao.PetDao;
import com.PetPeersApplication.dao.UserDao;
import com.PetPeersApplication.exception.UserValidation;
import com.PetPeersApplication.model.Pet;
import com.PetPeersApplication.model.User;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

	public String flagvalue = null;
	@Autowired
	private UserDao userDao;

	@Autowired
	private PetService petservice;

	@Override
	public User addUser(User user) throws UserValidation {

		User registeredUser = null;
		if (user.getUserName() != "") {
			List<User> users = listUsers();
			for (User userList : users) {
				if (!(userList.getUserName().equals(user.getUserName()))) {

					if (user.getUserPassword() != "") {

						if (user.getConfirmPassword() != "") {

							if (user.getUserPassword().equals(user.getConfirmPassword())) {
								registeredUser = userDao.save(user);
							} else {
								throw new UserValidation("Password should be match with ConfirmPassword");

							}

						} else {
							throw new UserValidation("ConfirmPassword should not be empty!!");

						}

					} else {
						throw new UserValidation("Password should not be empty!!");

					}
				} else {
					throw new UserValidation("User Name already in use. Please select a different User Name");

				}
			}
		} else {
			throw new UserValidation("UserName should not be empty!!");

		}

		return registeredUser;
	}

	@Override
	public User updateUser(User user) {
		return userDao.save(user);
	}

	@Override
	public List<User> listUsers() {
		List<User> users = userDao.findAll();
		return users;
	}

	@Override
	public User getUserById(long userId) {
		User user = userDao.getById(userId);
		return user;
	}

	@Override
	public User findByUserName(String userName) throws UserValidation {
		User user = userDao.findByUserName(userName);
		User resultUser = null;
		if (user != null) {
			flagvalue = userName;
			resultUser = user;
		} else {
			throw new UserValidation("Either User Name or Password or both are invalid");
		}

		return resultUser;
	}

	@Override
	public long removeUser(long userId) {
		userDao.deleteById(userId);
		return userId;
	}

	@Override
	public Pet buyPet(long petId) {

		List<User> users = listUsers();
		User owner = null;
		Pet pet = null;
		for (User user : users) {
			if (user.getUserName().equals(flagvalue)) {
				owner = user;
			}
		}

		List<Pet> pets = petservice.getAllPets();
		Set<Pet> resultSet = new HashSet<>();
		for (Pet petlist : pets) {
			if (petlist.getId() == petId) {
				petlist.setOwner(owner);
				resultSet.add(petlist);
				// LOGGER.info(petlist.getOwner());
				pet = petlist;
			}

			petservice.updatePet(petlist);
			owner.setPets(resultSet);
		}

		return pet;
	}

	@Override
	public Set<Pet> getmyPets() {
		Set<Pet> pets = new HashSet<>();
		List<User> users = listUsers();
		for (User user : users) {
			if (user.getUserName().equals(flagvalue)) {
				pets = user.getPets();

			}
		}

		return pets;

	}

}
