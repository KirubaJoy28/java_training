package com.petservice.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.petservice.model.Pet;
import com.petservice.service.PetService;

@RestController
@Validated
public class UserPetController {

	private static final Logger LOGGER = LogManager.getLogger(UserPetController.class);

	@Autowired

	private PetService petservice;

	@PostMapping(value = "Pets/addPet")
	public Pet addPet(@RequestBody @Valid Pet pet) {
		return petservice.savePet(pet);
	}

	@PutMapping(value = "Pets/updatePet")
	public Pet updatePet(@RequestBody @Valid Pet pet) {
		return petservice.updatePet(pet);
	}

	@GetMapping(value = "Pets/getAll")
	public List<Pet> getAll(Pet pet) {
		return petservice.getAllPets();
	}

	@GetMapping(value = "Pets/getOne/{var}")
	public Pet getOne(@PathVariable("var") long petId) {
		return petservice.getPetsById(petId);
	}

	@GetMapping(value = "Pets/petStatus/{petId}")
	public ResponseEntity<String> petStatus(@PathVariable("petId") long petId) {
		String status = null;
		Pet pet = petservice.getpetStatus(petId);
		if (pet != null) {
			status = "Available to buy";
		} else {
			status = "Already sold";
		}
		return new ResponseEntity<String>(status, HttpStatus.OK);
	}

	@GetMapping(value = "Pets/petStatusWithPet/{petId}")
	public Pet petStatusWithPet(@PathVariable("petId") long petId) {

		return petservice.getpetStatus(petId);
	}

}
