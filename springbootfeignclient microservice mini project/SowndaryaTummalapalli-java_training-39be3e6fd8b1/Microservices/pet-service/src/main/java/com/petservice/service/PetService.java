package com.petservice.service;

import java.util.List;

import com.petservice.model.Pet;


public interface PetService {

	public abstract Pet savePet(Pet pet);
	
	public abstract Pet updatePet(Pet pet);
	
	public abstract List<Pet> getAllPets();
	
	public abstract Pet getPetsById(long petId);
	
	public abstract Pet getpetStatus(long petId);
}


