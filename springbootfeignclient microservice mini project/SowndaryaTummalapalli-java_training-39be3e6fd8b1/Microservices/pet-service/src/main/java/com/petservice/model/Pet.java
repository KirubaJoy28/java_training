package com.petservice.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.ForeignKey;

@Entity
@Table(name = "PETS")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Pet implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@NotEmpty(message = "PetName should not be empty!!")
	private String petName;
	@Min(value = 0, message = "PetAge should be between 0 and 99")
	@Max(value = 99, message = "PetAge should be between 0 and 99")
	private int petAge;
	@NotEmpty(message = "petPlace should not be empty!!")
	private String petPlace;
	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public Pet() {
		super();
	}

	public Pet(long id, String petName, int petAge, String petPlace) {
		super();
		this.id = id;
		this.petName = petName;
		this.petAge = petAge;
		this.petPlace = petPlace;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public int getPetAge() {
		return petAge;
	}

	public void setPetAge(int petAge) {
		this.petAge = petAge;
	}

	public String getPetPlace() {
		return petPlace;
	}

	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}

}
