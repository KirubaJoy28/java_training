package com.petservice.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petservice.model.Pet;
import com.petservice.repository.PetRepository;

@Service

public class PetServiceImpl implements PetService {

	@Autowired
	private PetRepository petDao;

	@Override
	public Pet savePet(Pet pet) {

		return petDao.save(pet);

	}

	@Override
	public Pet updatePet(Pet pet) {

		return petDao.save(pet);
	}

	@Override
	public List<Pet> getAllPets() {
		
		return petDao.findAll();
	}

	@Override
	public Pet getPetsById(long petId) {
		
		return petDao.getById(petId);
	}

	@Override
	public Pet getpetStatus(long petId) {
		List<Pet> pets = getAllPets();
		Pet resultPet = null;
		for (Pet pet : pets) {
			if (pet.getId() == petId) {
				if (pet.getUserId() == 0) {
					resultPet = pet;
				} else {

				}
			}
		}

		return resultPet;
	}

}
