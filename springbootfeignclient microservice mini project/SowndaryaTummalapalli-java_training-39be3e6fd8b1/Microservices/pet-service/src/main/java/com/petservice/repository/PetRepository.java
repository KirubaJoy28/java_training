package com.petservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.petservice.model.Pet;



public interface PetRepository extends JpaRepository<Pet, Long>{

}
