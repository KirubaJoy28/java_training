package com.userservice.service;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.userservice.exception.UserValidation;
import com.userservice.model.Pet;
import com.userservice.model.User;
import com.userservice.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

	private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

	public String flagvalue = null;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PetFeignClient petFeignClient;

	@Override
	public User addUser(User user) throws UserValidation {

		User registeredUser = null;

		List<User> users = listUsers();
		for (User userList : users) {
			if (!(userList.getUserName().equals(user.getUserName()))) {

				if (user.getUserPassword().equals(user.getConfirmPassword())) {
					registeredUser = userRepository.save(user);
				} else {
					throw new UserValidation("Password should be match with ConfirmPassword");

				}

			} else {
				throw new UserValidation("User Name already in use. Please select a different User Name");

			}
		}

		return registeredUser;
	}
	
	@Override
	public User findByUserNameAndPassword(String userName, String userPassword) throws UserValidation {
		User user = null;
		Optional<User> users = userRepository.findByuserNameAnduserPassword(userName, userPassword);
		if (users.isPresent()) {
			user = users.get();
		}
		User resultUser = null;
		if (user != null) {
			flagvalue = userName;
			resultUser = user;
		} else {
			throw new UserValidation("Either User Name or Password or both are invalid");
		}

		return resultUser;
	}


	@Override
	public List<User> listUsers() {

		return userRepository.findAll();
	}

	@Override
	public Pet buyPet(long petId) {
		List<User> users = listUsers();
		User owner = null;
		Pet pet = null;
		for (User user : users) {
			if (user.getUserName().equals(flagvalue)) {
				owner = user;
			}
		}

		List<Pet> pets = petFeignClient.getAll();
		Set<Pet> resultSet = new HashSet<>();
		for (Pet petlist : pets) {
			if (petlist.getId() == petId) {
				petlist.setUserId(owner.getUserId());
				resultSet.add(petlist);
				// LOGGER.info(petlist.getOwner());

				pet = petlist;
				petFeignClient.updatePet(petlist);
			}

			//owner.setPets(resultSet);

		}

		return pet;
	}

	@Override
	public Set<Pet> getMyPets() {
		Set<Pet> pets = new HashSet<>();
		List<User> users = listUsers();
		List<Pet> petlist = petFeignClient.getAll();
		for (User user : users) {
			if (user.getUserName().equals(flagvalue)) {
				for (Pet pet : petlist) {
					if (user.getUserId() == pet.getUserId()) {
						pets.add(pet);
					}
				}

			}
		}

		return pets;

	}
}
