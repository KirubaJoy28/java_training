package com.userservice.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.userservice.model.Pet;

@FeignClient(name = "abc", url = "http://localhost:9092/PetUser/")
public interface PetFeignClient {

	@PostMapping(value = "Pets/addPet")
	public abstract Pet addPet(@RequestBody  Pet pet);

	@PutMapping(value = "Pets/updatePet")
	public abstract Pet updatePet(@RequestBody  Pet pet);

	@GetMapping(value = "Pets/getAll")
	public abstract List<Pet> getAll();

	@GetMapping(value = "Pets/petStatus/{petId}")
	public ResponseEntity<String> petStatus(@PathVariable("petId") long petId);

	@GetMapping(value = "Pets/getOne/{var}")
	public Pet getOne(@PathVariable("var") long petId);
	
	@GetMapping(value = "Pets/petStatusWithPet/{petId}")
	public Pet petStatusWithPet(@PathVariable("petId") long petId);

}
