package com.userservice.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.userservice.model.User;



public interface UserRepository extends JpaRepository<User, Long>{

	 @Query("SELECT t FROM User t where t.userName = ?1 AND t.userPassword = ?2")
		public abstract Optional<User> findByuserNameAnduserPassword(String username,String userPassword);
		
		
}
