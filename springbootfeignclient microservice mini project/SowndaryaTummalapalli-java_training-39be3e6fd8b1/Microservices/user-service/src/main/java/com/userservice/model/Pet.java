package com.userservice.model;

import java.io.Serializable;

import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class Pet implements Serializable {

	private long id;

	private String petName;
	private int petAge;
	private String petPlace;

	private long userId;

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "Pet [id=" + id + ", petName=" + petName + ", petAge=" + petAge + ", petPlace=" + petPlace + "]";
	}

	public Pet() {
		super();
	}

	public Pet(long id, String petName, int petAge, String petPlace) {
		super();
		this.id = id;
		this.petName = petName;
		this.petAge = petAge;
		this.petPlace = petPlace;

	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPetName() {
		return petName;
	}

	public void setPetName(String petName) {
		this.petName = petName;
	}

	public int getPetAge() {
		return petAge;
	}

	public void setPetAge(int petAge) {
		this.petAge = petAge;
	}

	public String getPetPlace() {
		return petPlace;
	}

	public void setPetPlace(String petPlace) {
		this.petPlace = petPlace;
	}

}
