package com.userservice.service;

import java.util.List;
import java.util.Set;

import com.userservice.exception.UserValidation;
import com.userservice.model.Pet;
import com.userservice.model.User;

public interface UserService {

	public abstract User addUser(User user);

	public abstract Pet buyPet(long petId);

	public abstract List<User> listUsers();

	public abstract Set<Pet> getMyPets();// getMyPets
	
	public abstract User findByUserNameAndPassword(String userName,String userPassword) throws UserValidation;

	/*
	 * public abstract User updateUser(User user);
	 * 
	 * 
	 * 
	 * public abstract User getUserById(long userId);
	 * 
	 * public abstract User findByUserName(String userName);
	 * 
	 * public abstract User findByUserNameAndPassword(String userName,String
	 * userPassword);
	 * 
	 * public abstract long removeUser(long userId);
	 * 
	 * // public abstract Pet buyPet(long petId);
	 * 
	 * //
	 */

}
