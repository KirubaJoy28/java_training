package com.userservice.exception;

public class UserValidation extends RuntimeException {

	private String message;

	
	public UserValidation() {
		super();
	}

	public UserValidation(String message) {
		super();
		this.message = message;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	
}
