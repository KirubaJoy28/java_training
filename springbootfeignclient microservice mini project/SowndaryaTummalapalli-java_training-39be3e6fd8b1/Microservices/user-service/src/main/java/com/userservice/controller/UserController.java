package com.userservice.controller;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.userservice.model.Pet;
import com.userservice.model.User;
import com.userservice.service.PetFeignClient;
import com.userservice.service.UserService;

@RestController
@Validated

public class UserController {

	private static final Logger LOGGER = LogManager.getLogger(UserController.class);

	@Autowired
	private UserService userservice;

	@Autowired

	private PetFeignClient petFeignClient;

	@PostMapping(value = "user/add")
	public User addUser(@RequestBody @Valid User user) {
		return userservice.addUser(user);
	}

	@GetMapping(value = "user/login/{username}/{password}")
	public ResponseEntity<String> loginUser(@PathVariable("username") String username,
			@PathVariable("password") String password) {
		User user = userservice.findByUserNameAndPassword(username, password);
		String result = null;
		if (user != null) {
			result = "Welcome " + user.getUserName();

		}

		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	@PostMapping(value = "Pets/addPet")
	public Pet addPet(@RequestBody @Valid Pet pet) {
		return petFeignClient.addPet(pet);
	}

	@PutMapping(value = "Pets/updatePet")
	public Pet updatePet(@RequestBody Pet pet) {
		return petFeignClient.updatePet(pet);
	}

	@GetMapping(value = "Pets")
	public List<Pet> getAll() {
		return petFeignClient.getAll();
	}

	@GetMapping(value = "Pets/buyPet/{petId}")
	public ResponseEntity<String> buyPet(@Min(value=1,message="petId should be greater than 0") @PathVariable("petId") long petId) {
		String result = null;
		Pet pet = petFeignClient.petStatusWithPet(petId);
		if (pet != null) {
			userservice.buyPet(petId);
			result = "Pet " + pet.getPetName() + " bought successfully";
		} else {
			result = "Already sold";
		}
		return new ResponseEntity<String>(result, HttpStatus.OK);

	}

	@GetMapping(value = "Pets/petStatus/{petId}")
	public ResponseEntity<String> petStatus(@PathVariable("petId") long petId) {

		return petFeignClient.petStatus(petId);

	}

	@GetMapping(value = "Pets/myPets")
	public ResponseEntity<Set<Pet>> getMyPets() {
		Set<Pet> pets = userservice.getMyPets();

		Set<Pet> setOfPets = new HashSet<>();

		Pet resultPet = null;
		if (pets != null) {
			for (Pet pet : pets) {
				resultPet = new Pet(pet.getId(), pet.getPetName(), pet.getPetAge(), pet.getPetPlace());
				setOfPets.add(resultPet);
			}
		}

		return new ResponseEntity<Set<Pet>>(pets, HttpStatus.OK);
	}

}