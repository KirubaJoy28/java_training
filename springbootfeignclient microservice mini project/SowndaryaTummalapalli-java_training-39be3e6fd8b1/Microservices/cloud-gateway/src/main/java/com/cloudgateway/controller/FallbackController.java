package com.cloudgateway.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FallbackController {
	@GetMapping("/petServiceFallBack")
	public String petServiceFallBackMethod() {
		return "Pet Service is taking longer than Expected." + " Please try again later";
	}

	@GetMapping("/userServiceFallBack")
	public String userServiceFallBackMethod() {
		return "User Service is taking longer than Expected." + " Please try again later";
	}
}
